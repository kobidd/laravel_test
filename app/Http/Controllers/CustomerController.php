<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id=Auth::id();
        $customers=Customer::all();
        //$sales = DB::table('users')->where('name',$id)->first();
        //$idn = $sales;
        //$user = User::find($idn);
        //$customers= User::find($id)->customers;
        //$customers->saleName = $user;
        //$id1 = $customers->user_id;
      //  $user_name = User::find($id1)->name; 
       // $customers->user_id = $user_name;
        return view('customers.index',['customers'=>$customers],['id'=>$id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id=Auth::id();
        $customers = new Customer();
        $customers->name = $request->name;
        $customers->email = $request->email;
        $customers->phone = $request->phone;
        $customers->user_id = $id;
        $customers->save();
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            $id1=Auth::id();
            $customers1=Customer::find($id);
            if($id1==$customers1->user_id){
            return view('customers.edit',['customers'=>$customers1]);
            }
            abort(403,"Sorry you do not hold permission to edit this customer");
        }
        $customers=Customer::find($id);
        return view('customers.edit',['customers'=>$customers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customers = Customer::find($id);
        $customers -> update($request->all());
        return redirect('customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to delete customers");
        } 

        $customers = Customer::find($id);
        $customers -> delete();
        return redirect('customers');
    }
    public function upd($id){
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to update customers");
        } 
        $customers = Customer::find($id);
        $customers->status =1;
        $customers->update();   
        return redirect('customers');
    }
}
