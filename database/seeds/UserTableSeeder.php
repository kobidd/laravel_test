<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'manager',
                        'email' => 'a@a.com',
                        'password' => Hash::make('12345678'),
                        'created_at' => date('Y-m-d G:i:s'),
                        'role'=>'manager',
                ],
                [
                        'name' => 'salesrp1',
                        'email' => 'b@b.com',
                        'password' => Hash::make('12345678'),
                        'created_at' => date('Y-m-d G:i:s'),
                        'role'=>'salesrep',
                ],
                [
                        'name' => 'salesrp2',
                        'email' => 'c@c.com',
                        'password' => Hash::make('12345678'),
                        'created_at' => date('Y-m-d G:i:s'),
                        'role'=>'salesrep',
                ],
            
                    ]);
    }
}
