@extends('layouts.app')
@section('content')
<h1>Edit Task</h1>
<form method = 'post' action="{{action('CustomerController@update', $customers->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "name">Customer Name to Update:</label>
    <input type= "text" class = "form-control" name= "name" value = "{{$customers->name}}">
</div>
<div class = "form-group">
    <label for = "email">Customer Email to Update:</label>
    <input type= "email" class = "form-control" name= "email" value = "{{$customers->email}}">
</div>
<div class = "form-group">
    <label for = "phone">Customer Phone Number to Update:</label>
    <input type= "number" class = "form-control" name= "phone" value = "{{$customers->phone}}">
</div>
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Just Do It">
</div>
</form>
@endsection