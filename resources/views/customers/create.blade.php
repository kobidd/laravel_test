@extends('layouts.app')
@section('content')
<h1>Create New Customer</h1>
<form method = 'post' action="{{action('CustomerController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "name">Client Name</label>
    <input type= "text" class = "form-control" name= "name">
</div>
<div class = "form-group">
    <label for = "email">Client email</label>
    <input type= "email" class = "form-control" name= "email">
</div>
<div class = "form-group">
    <label for = "phone">Client Phone Number</label>
    <input type= "number" class = "form-control" name= "phone">
</div>
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Just Do It">
</div>

</form>
@endsection