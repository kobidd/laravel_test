@extends('layouts.app')
@section('content')
<h1>This is your customers list</h1>
<a href= "{{route('customers.create', $id)}}">Create New Customer</a>
<table>
<tr>
    <th>Name</th>
    <th>Email</th>
    <th>Phone Number</th>
    <th>Salesman Name</th>
</tr>
    @foreach($customers as $customer)
    @if($customer->status)
        @if($customer->user_id == $id)
            <tr>
            <td style="color: green; font-weight:bold">{{$customer->name}}</td>
            <td style="color: green; font-weight:bold">{{$customer->email}}</td>
            <td style="color: green; font-weight:bold">{{$customer->phone}}</td> 
            <td style="color: green; font-weight:bold">{{$customer->user->name}}</td> 
            <td><a href= "{{route('customers.edit', $customer->id )}}">Edit Customer</a></td>
            <td>@cannot('salesrep')<a href="{{route('delete',$customer->id)}}">Delete Client</a>@endcannot<td>
            </tr>  
         @else
            <tr>
            <td style="color: green;">{{$customer->name}}</td>
            <td style="color: green;">{{$customer->email}}</td>
            <td style="color: green;">{{$customer->phone}}</td> 
            <td style="color: green;">{{$customer->user->name}}</td> 
            <td><a href= "{{route('customers.edit', $customer->id )}}">Edit Customer</a></td>
            <td>@cannot('salesrep')<a href="{{route('delete',$customer->id)}}">Delete Client</a>@endcannot<td>
            </tr>  
        @endif
    @elseif($customer->user_id == $id)
    <tr>
    <td style= "font-weight:bold">{{$customer->name}}</td>
    <td style= "font-weight:bold">{{$customer->email}}</td>
    <td style= "font-weight:bold">{{$customer->phone}}</td> 
    <td style= "font-weight:bold">{{$customer->user->name}}</td> 
    <td><a href= "{{route('customers.edit', $customer->id )}}">Edit Customer</a></td>
    <td>@cannot('salesrep')<a href="{{route('delete',$customer->id)}}">Delete Client</a>@endcannot</td>
    <td>@cannot('salesrep')<a href="{{route('update',$customer->id)}}">Deal Closed</a>@endcannot</td>
    </tr> 
    @else
    <tr>
    <td>{{$customer->name}}</td>
    <td>{{$customer->email}}</td>
    <td>{{$customer->phone}}</td> 
    <td>{{$customer->user->name}}</td> 
    <td><a href= "{{route('customers.edit', $customer->id )}}">Edit Customer</a></td>
    <td>@cannot('salesrep')<a href="{{route('delete',$customer->id)}}">Delete Client</a>@endcannot</td>
    <td>@cannot('salesrep')<a href="{{route('update',$customer->id)}}">Deal Closed</a>@endcannot</td>
    </tr>  
    @endif 
@endforeach
</table>
@endsection